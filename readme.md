# BeatCorp plugin

## Installation

To start the plugin in dev mode, type `npm run serve` in the terminal. The manifest.json will be in the `dist` folder.
To build the plugin for production, type `npm run build` in the terminal. The build will be in the `dist` folder.

## Overview

The plugin uses npm to manage packages. It works with VueJS3 for the popup part.

## Dependencies

Axios 0.20.0
Core-js 3.8.3
vue 2.6.14
vue-router 3.5.1
vuex 3.6.2

## Popup

The popup includes four components: App.vue, LoginComponent.vue, LogoutComponent.vue and ScrapingComponent.vue. It also uses services to link with the API (via Axios).

### Components

#### App.vue

The template includes a main header with the plugin name and a logout button if the user is logged in. Below this is a section containing the loginComponent (if the user is logged out) and the scrapingComponent (if the user is logged in).
When the component is mounted (i.e. when the popup is opened), the `onOpenPopup()` method is called and an event listener is opened with the `popupListener()` method as callback (it will be destroyed at the same time as the component).
`onOpenPopup()` sends an `openPopup` message to the background script.
`popupListener(request, send, sendResponse)` calls the `isConnected()` method if the action is `login`.
`isConnected()` checks the access token (JSONWebToken) and adapts the view accordingly.

#### LoginComponent.vue

The template includes a form with two inputs (a text and a password), and a button calling the `login()` method.
The `login()` method retrieves the credentials and calls the `AuthService.login(credentials)` method, emitting a boolean depending on the result of the login request (this boolean will be retrieved by the App.vue).

#### LogoutComponent.vue

The template includes a single link calling the `logout()` method.
The `logout()` method calls the `AuthService.logout()` method and emits a boolean depending on the result of the logout request (this boolean will be retrieved by the App.vue).

#### ScrapingComponent.vue

The template includes a button calling the `grabHTML()` method, a paragraph displaying the amount of data collected and a section displaying when collection is complete and offering to retrieve the data with the `generateJSON()` method, another allowing data to be sent to the API with the `sendDataToAPI()` method, and a final section to reset the component `state = null`. A final section displays a message if the user attempts to scrape a page not supported by the plugin.
When the component is mounted, it listens for messages from the background script (this listener is destroyed when the component is unmounted).
The `grabHTML()` method sends a message to the background script with the action `scrap`.
The `generateJSON()` method generates a .json file and downloads it to the user's machine.
The `sendDataToAPI()` method uses `DataService.post(data)` to send data to the API.
The `receiveData(request, sender, sendResponse)` method processes messages from the background script. If the action is `data`, the data is passed to `dataTreatment(data)`. If the action is `end`, the component updates itself and proposes downloading and sending data to the API to the user. If the action is `resume`, this means that the popup was closed during scraping and that the data stored in the background script must be retrieved and offered to the user.
The `dataTreatment(data)` method sorts the data to build a coherent corpus that can be used by the API.

### Services

#### http-common

Creates an Axios instance pointing to the API. The headers specify the content-type (application/json), withCredentials is true. The instance is then exported.

#### AuthenticationService

The AuthenticationService class includes two methods:
`login(credentials)` uses the Axios post method and transmits the user's credentials to the `api/auth/login` API endpoint.
`logout()` uses the Axios post method and forwards the request to the `api/auth/logout` API endpoint.

#### DataService

The DataService class has a single method:
`post(data)` uses the Axios post method and transmits the data collected by the user to the `api/plug/handle_scraping` endpoint.

## Background script

The script listens to the various messages coming from the popup and the content script.
`scrap`: sends the content script the command to start scraping.
`data`: stores scraped data sent by the content script in the localStorage.
`download`: deletes scraped data from the localStorage.
`loggedIn`: stores the current timestamp in the localStorage, checks whether scraped data is stored in the localStorage and sends it to the ScrapingComponent with the `resume`action.
`openPopup`: retrieves the timestamp stored in the localStorage, converts it to minutes and subtracts it from the current timestamp. If the difference is less than 30 minutes, the access token is still valid and the user can continue, but the stored timestamp is updated. Otherwise, the user must log in again.
`logout`: empties the localStorage.
end`: changes the plugin icon to indicate to the user that scraping is complete.

The `changeIcon(iconName)` function modifies the icon.

## Content script

The script listens to messages from the background script and the popup. If the action is `scrap` and the host name corresponds to a page supported by the plugin (currently `app.beesbusy.com`, `www.notion.so`, `trello.com` and `kbskilliut`), the script retrieves the predefined elements and sends them as a packet to the background script. When collection is complete, an event with the action `end` is sent to signal the end of processing.
