browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === "scrap") {
        browser.tabs.query({ active: true, currentWindow: true }, (tabs) => {
            const activeTab = tabs[0];
            browser.tabs.sendMessage(activeTab.id, { action: "scrap" });
        });
    }
    if (request.action === "data") {
        window.localStorage.setItem([`catcap_${Date.now()}`], JSON.stringify(request.result));
    }
    if (request.action === "download") {
        for (let key in window.localStorage) {
            if (key.startsWith("catcap_") && key !== "catcap_timestamp") {
                window.localStorage.removeItem([key]);
            }
        }
        changeIcon("icon");
    }
    if (request.action === "loggedIn") {
        window.localStorage.setItem("catcap_timestamp", Date.now())
        const data = [];
        for (let key in window.localStorage) {
            if (key.startsWith("catcap_") && key !== "catcap_timestamp") {
                data.push(window.localStorage.getItem([key]));
            }
        }
        if (data.length > 0) {
            browser.runtime.sendMessage({ action: "resume", result: data })
        }
    }
    if (request.action === "openPopup") {
        const timestampJWT = window.localStorage.getItem("catcap_timestamp");

        if (timestampJWT) {
            const deltaJWT = (Date.now() - timestampJWT) / 60000 // convert timestamp to minutes and get difference between last connection and now

            if (deltaJWT <= 30) {
                browser.runtime.sendMessage({ action: "login", status: true });
                const data = [];
                for (let key in window.localStorage) {
                    if (key.startsWith("catcap_") && key !== "catcap_timestamp") {
                        data.push(window.localStorage.getItem([key]));  
                    }
                }
                if (data.length > 0) {
                    browser.runtime.sendMessage({ action: "resume", result: data })
                }
            }
        } else {
            browser.runtime.sendMessage({ action: "login", status: false });
        }
    }
    if (request.action === "logout") {
        window.localStorage.clear();
    }
    if (request.action === "end") {
        changeIcon("icon_alt");
    }
    return Promise.resolve();
});

function changeIcon(iconName) {
    browser.browserAction.setIcon({ path: `../icons/${iconName}.svg` })
}