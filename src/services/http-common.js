import axios from "axios";

const apiClient = axios.create({
    baseURL: "https://catcap.univ-smb.fr/",
    //baseURL: "http://localhost:8000/",
    headers: {
        "Content-Type": "application/json"
    },
    withCredentials: true
});

apiClient.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response.status === 401) {
            return Promise.reject(new Error('unauthorized'));
        }
        return Promise.reject(error);
    }
)

export default apiClient;