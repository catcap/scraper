import http from "./http-common.js";

class DataService {
    post(data) {
        return http.post("api/plug/handle_scraping", data);
    }
}

export default new DataService();