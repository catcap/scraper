import http from "./http-common.js";

class AuthenticationService {
    login(credentials) {
        return http.post("api/auth/login", credentials);
    }

    async refresh() {
        let isLogged;
        await http.get("api/auth/me")
        .then((res) => {
            isLogged = true;
        })
        .catch((error) => {
            isLogged = false;
        });
        return isLogged;
    }

    async logout() {
        return await http.get("api/auth/logout", {
            headers: {
                "Content-Type": "multipart/form-data",
            }
        });
    }
}

export default new AuthenticationService();
