import BeesbusyScraper from "./Scrapers/BeesbusyScraper.js";
import KBSkillScraper from "./Scrapers/KBSkillScraper.js";
import NotionScraper from "./Scrapers/NotionScraper.js";
import TrelloScraper from "./Scrapers/TrelloScraper.js";

browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.msg && request.msg.action === "scrap") {
        if (window.location.hostname && window.location.hostname === "app.beesbusy.com") {
            const tasks = document.querySelectorAll(
                "div[data-css-bweh18]"
            );
            const beesbusyScraper = new BeesbusyScraper(browser, tasks);
            beesbusyScraper.scrapStart();
        } else if (window.location.hostname && window.location.hostname === "www.notion.so") {
            const corpusTitle = document.querySelector("div.notion-selectable.notion-collection_view_page-block h1.notranslate").innerHTML;
            const activities = document.querySelectorAll("div.notion-selectable.notion-page-block.notion-timeline-item a");
            const notionScraper = new NotionScraper(browser, corpusTitle, activities);
            notionScraper.scrapStart();
        } else if (window.location.hostname && window.location.hostname === "trello.com") {
            const corpusTitle = document.querySelector(".HKTtBLwDyErB_o").innerHTML;
            const activities = document.querySelectorAll("li.T9JQSaXUsHTEzk > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)");
            const trelloScraper = new TrelloScraper(browser, corpusTitle, activities);
            trelloScraper.scrapStart();
        } else if (window.location.href && window.location.href.includes("kbskilliut")) {
            const corpusTitle = document.querySelector(".title").innerHTML.replace("\n", "").trim();
            const activities = Array.from(document.querySelectorAll("div.task-board-header > a.js-modal-large")).filter(activity => activity.title === "");
            const kbskillScraper = new KBSkillScraper(browser, corpusTitle, activities);
            kbskillScraper.scrapStart();
        } else {
            browser.runtime.sendMessage({ action: "wrong_page" });
        }

    }
});




