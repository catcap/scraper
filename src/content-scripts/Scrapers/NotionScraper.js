export default class NotionScraper {
    constructor(browser, corpusTitle, activities) {
        this.browser = browser;
        this.corpusTitle = corpusTitle;
        this.activities = activities;
        this.scrapEndEvent = new Event("scrapEnd");
        document.addEventListener("scrapEnd", this.scrapEnd);
    }

    scrapEnd = () => {
        this.browser.runtime.sendMessage({ action: "end" });
        document.removeEventListener("scrapEnd", this.scrapEnd);
    }

    scrapStart = () => {
        this.openTask(0, this.activities);
    }

    openTask = (i, tasks) => {
        if (i >= tasks.length) {
            document.dispatchEvent(this.scrapEndEvent);
        } else {
            tasks[i].click();
            setTimeout(() => {
                const { activity, date, details, actions, actors } = this.scrapTask();
                actions.forEach(action => {
                    const data = {
                        corpusName: this.corpusTitle,
                        activity: activity,
                        task: action,
                        date: date,
                        details: details,
                        actions: [],
                        actors: actors
                    };
                    this.browser.runtime.sendMessage({
                        action: "data",
                        data,
                    })
                })
                i++;
            }, 2000);
            setTimeout(() => this.openTask(i, tasks), 2500)
        }
    }

    scrapTask = () => {
        const activity = document.querySelector("div.layout.layout-side-peek div.layout-content div div div.notion-selectable.notion-page-block h1.notranslate").innerHTML;
        const date = document.querySelector("div.layout-content:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1)").lastChild.lastChild.lastChild.lastChild.lastChild.innerHTML;
        const details = Array.from(document.querySelectorAll("div.notion-selectable.notion-page-block div div div div div div div div div div div")).map(detail => detail.innerHTML);
        const actions = Array.from(document.querySelectorAll("div.notion-selectable.notion-to_do-block div div div div.notranslate")).map(action => action.innerHTML);
        const actors = [...new Set(Array.from(document.querySelectorAll("div.notion-selectable.notion-page-block div div div div div div div span")).map(actor => actor.innerHTML))]
        return { activity, date, details, actions, actors };
    }
}