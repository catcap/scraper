export default class TrelloScraper {
    constructor(browser, corpusTitle, activities) {
        this.browser = browser;
        this.corpusTitle = corpusTitle;
        this.activities = activities;
        this.scrapEndEvent = new Event("scrapEnd");
        document.addEventListener("scrapEnd", this.scrapEnd);
    }

    scrapEnd = () => {
        this.browser.runtime.sendMessage({ action: "end" });
        document.removeEventListener("scrapEnd", this.scrapEnd);
    };

    scrapStart = () => {
        this.openTask(0, this.activities);
    };

    openTask = async (i, tasks) => {
        if (i >= tasks.length) {
            document.dispatchEvent(this.scrapEndEvent);
        } else {
            tasks[i].addEventListener("click", () => {
                setTimeout(() => {
                    const { activity, date, details, actions, actors } =
                        this.scrapTask();
                    actions.forEach((action) => {
                        const data = {
                            corpusName: this.corpusTitle,
                            activity: activity,
                            task: action,
                            date: date,
                            details: details,
                            actions: [],
                            actors: actors,
                        };
                        this.browser.runtime.sendMessage({
                            action: "data",
                            data,
                        });
                    });
                }, 1000);
            });
            tasks[i].click();

            setTimeout(() => {
                i++;
                this.openTask(i, tasks);
            }, 5000);
        }
    };

    scrapTask = () => {
        const activity = document.querySelector(".mod-card-back-title").value;
        const actions = Array.from(
            document.querySelectorAll(".checklist-item-details-text")
        ).map((action) => action.innerHTML);
        const details = document.querySelector(".current").innerText.split(/\s*\n\s*(?=\b(?:ressource|objectif)s?:?\b)/gi).map(detail => detail.replace(/\n/g, " ").replace(/\s+/g, ' ').trim());
        const date = document.querySelector(".mc4MOOMqrYgKej > span:nth-child(1)").innerHTML;
        const actors = Array.from(document.querySelectorAll("button.member > :first-child")).map(actor => actor.title.replace(/\s*\(.*?\)\s*/, ''));
        return { activity, date, details, actions, actors };
    };
}
