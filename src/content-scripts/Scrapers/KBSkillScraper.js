export default class KBSkillScraper {
    constructor(browser, corpusTitle, activities) {
        this.browser = browser;
        this.corpusTitle = corpusTitle;
        this.activities = activities;
        this.scrapEndEvent = new Event("scrapEnd");
        document.addEventListener("scrapEnd", this.scrapEnd);
    }

    scrapEnd = () => {
        this.browser.runtime.sendMessage({ action: "end" });
    };

    scrapStart = () => {
        this.openTask(0, this.activities);
    };

    openTask = (i, tasks) => {
        if (i >= tasks.length) {
            document.dispatchEvent(this.scrapEndEvent);
        } else {
            tasks[i].click();
            setTimeout(() => {
                const { activity, task, date, details, actors } = this.scrapTask();
                const data = {
                    corpusName: this.corpusTitle,
                    activity: activity,
                    task: task,
                    date: date,
                    details: details,
                    actions: [],
                    actors: actors,
                };
                this.browser.runtime.sendMessage({
                    action: "data",
                    data,
                });
                i++;
            }, 2000);
            setTimeout(() => this.openTask(i, tasks), 2500);
        }
    };

    scrapTask = () => {
        const activity = document
            .querySelector(".page-header > h2:nth-child(1)")
            .innerText.split(" > ")[0];
        const task = document.querySelector("#form-title").value;
        let date = document.querySelector("#form-date_started").value.split(" ")[0]
        if (!date) {
            date = document.querySelector("#form-date_due").value.split(" ")[0] ? document.querySelector("#form-date_due").value.split(" ")[0] : `${new Date().getDate().toString().padStart(2, "0")}/${new Date().getMonth().toString().padStart(2, "0")}/${new Date().getFullYear().toString().padStart(4, "0")}`;
        }
        let details = Array.from(
            document.querySelectorAll(
                "span.select2:nth-child(8) > span:nth-child(1) > span:nth-child(1) > ul:nth-child(1) > li"
            )
        )
            .map((detail) => detail.title)
            .filter((detail) => detail != "");
        Array.from(
            document.querySelectorAll(
                ".skill-task > span:nth-child(5) > span:nth-child(1) > span:nth-child(1) > ul:nth-child(1) > li"
            )
        ).forEach((detail) => details.push(detail.title));
        details.push(
            document.querySelector(
                ".text-editor-write-mode > textarea:nth-child(2)"
            ).value
        );
        details = details.filter((elem, index) => {
            return details.indexOf(elem) === index;
        });
        const actors = Array.from(
            document.querySelectorAll(
                "span.select2:nth-child(11) > span:nth-child(1) > span:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
            )
        ).map((actor) => actor.title).filter(actor => actor !== "");
        actors.push(Array.from(document.querySelector("#form-owner_id").childNodes).filter(actor => actor.selected).map(actor => actor.innerHTML).join());
        return { activity, task, date, details, actors };
    };
}
