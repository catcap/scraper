export default class BeesbusyScraper {
    constructor(browser, tasks) {
        this.browser = browser;
        this.tasks = tasks;
        this.totalParts = this.tasks.length;
        this.totalTasks = [];
        this.scrapEndEvent = new Event("scrapEnd");
        this.totalInput = 0;
        this.observer = new MutationObserver((mutationsList) => {
            for (let mutation of mutationsList) {
                if (mutation.type === "childList") {
                    const childNodes = mutation.target.lastChild.childNodes;
                    this.totalTasks.push(childNodes);
                    if (this.totalTasks.length === this.totalParts) {
                        setTimeout(() => {
                            this.totalInput = this.totalTasks.reduce(
                                (total, part) => total + part.length,
                                0
                            );
                            this.clickOnTask();
                        }, 2000);
                    }
                }
            }
        });
        
        document.addEventListener("scrapEnd", this.scrapEnd);
    }

    scrapEnd = () => {
        this.observer.disconnect();
        this.browser.runtime.sendMessage({ action: "end" });
        document.removeEventListener("scrapend", this.scrapEnd);
    }

    scrapStart = () => {
        this.tasks.forEach((elem) => {
            this.observer.observe(elem.parentElement, {
                attributes: true,
                childList: true,
            });
            elem.click();
        })
    }

    clickOnTask = () => {
        let totalProcess = 0;
        let partCounter = 0;
        let taskCounter = 0;
        let interval = setInterval(() => {
            this.totalTasks[partCounter]
                .item(taskCounter)
                .querySelector("div[data-css-1hcx6qb")
                .addEventListener("click", () =>
                    setTimeout(() => this.processTask(taskCounter), 500)
                );
            this.totalTasks[partCounter]
                .item(taskCounter)
                .querySelector("div[data-css-1hcx6qb")
                .click();
            taskCounter++;
            totalProcess++;
            if (taskCounter === this.totalTasks[partCounter].length) {
                partCounter ++;
                taskCounter = 0;
            }
            if (totalProcess === this.totalInput) {
                clearInterval(interval);
                setTimeout(() => document.dispatchEvent(this.scrapEndEvent), 5000);
            }
        }, 2500);
    }

    processTask = () => {
        const corpusName = document.querySelector("div[data-css-1x6au4g]").innerHTML.split(" - #")[0];
        const activity = document.querySelector("span.Select-value-label").innerHTML;
        const task = document.querySelector("div[data-css-1htjggs]").innerHTML;
        const date = document.querySelector("div[data-css-1erdh0h]").innerHTML;
        const buttons = document.querySelectorAll("button[data-css-1f3y3qn]");
        buttons[buttons.length - 1].click();
        const actors = [];
        setTimeout(() => {
            actors.push(
                ...Array.from(
                    document.querySelectorAll("span[data_css_1qga70y]")
                ).map((actor) => actor.innerHTML)
            )
        }, 500);
        const actions = Array.from(
            document.querySelectorAll("div[data-css-j2ponf]")
        ).map((action) => action.innerHTML);
        const details = Array.from(
            document.querySelectorAll("div[data-css-1q02bzu]")
        ).map((detail) =>
            detail.textContent
        );

        setTimeout(() => {
            const data = {
                corpusName: corpusName,
                activity: activity,
                task: task,
                date: date,
                details: details,
                actions: actions,
                actors: actors,
            };
            this.browser.runtime.sendMessage({
                action: "data",
                data,
            })
        }, 1400);
    }
}